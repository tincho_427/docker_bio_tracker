## Install Docker

Run the installation file:  

```./install_docker```  

## Create the docker container

The docker conntainer install all requirements to run the *bio_tracker* project. For install it:  

```sudo docker build -t bio_tracker .```  

## Configure the docker container  

First of all you have to modify the *run_docker* file in the line 8, where you have to change the username for your own username.  

Next, create a folder ```/home/your_username/``` with the name ```bio_tracker_files```. Every video you want to analyzate have to ve here. And in the docker you access to this folder in ```/home/bio_tracker/files/```.  

## Run the container  

For run the docker container:  

```./run_docker```

When you want to exit type ```ctrl```+```d```