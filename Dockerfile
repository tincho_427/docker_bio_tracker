FROM ubuntu:16.04
MAINTAINER Martin Reyes

RUN apt-get update
RUN apt-get install -y build-essential apt-utils

RUN apt-get install -y cmake git libgtk2.0-dev pkg-config libavcodec-dev \
  libavformat-dev libswscale-dev

RUN  apt-get update && apt-get install -y python-dev \
  python3 python3-pip python3-dev libtbb2 libtbb-dev \
  libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev \
  python-opencv libopencv-dev libav-tools \
  qt5-default python3-tk

RUN cd ~/ &&\
    git clone https://github.com/Itseez/opencv.git &&\
    git clone https://github.com/Itseez/opencv_contrib.git &&\
    cd opencv && mkdir build && cd build && cmake  -DWITH_QT=ON -DWITH_OPENGL=ON -DFORCE_VTK=ON -DWITH_TBB=ON -DWITH_GDAL=ON -DWITH_XINE=ON -DBUILD_EXAMPLES=ON .. && \
    make -j4 && make install && ldconfig && rm -rf ~/opencv*  # Remove the opencv folders to reduce image size

RUN cd /home && \
    git clone https://tincho_427@bitbucket.org/tincho_427/bio_tracker.git && \
    cd bio_tracker && \
    mkdir build && \
    cd build && \
    cmake .. && \
    make -j4 && \
    cd .. && \
    pip3 install -r requirements.txt

RUN mkdir -p /home/bio_tracker/files

WORKDIR /home/bio_tracker
